import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:test_haupcar_application/Screens/homeScreen.dart';

import 'Screens/productSreen.dart';

void main() async{
  WidgetsFlutterBinding.ensureInitialized();
  await EasyLocalization.ensureInitialized();

  runApp( EasyLocalization(
    supportedLocales: const [Locale('en', 'US'),Locale('th', 'TH')],
    path: 'assets/lang',
    fallbackLocale: const Locale('th', 'TH'),
    child: MyApp()));
}

class MyApp extends StatelessWidget {
  MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      localizationsDelegates: context.localizationDelegates,
      supportedLocales: context.supportedLocales,
      locale: context.locale,
      initialRoute: '/',
      getPages: [
         GetPage(name: '/', page: () => HomeScreen()),
          GetPage(name: '/productList', page: () => ProductListScreen())
      ], 
    );
  }
}
