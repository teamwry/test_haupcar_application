import 'package:get/get.dart';
import 'package:test_haupcar_application/Services/services.dart';

class CategoreiesController extends GetxController{
  final api = API();
  var categoriesList = [].obs;

  callCategories()async{
    var response = await api.categories();
    categoriesList.value = response;
  }
}