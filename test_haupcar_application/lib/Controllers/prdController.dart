import 'package:get/get.dart';
import 'package:test_haupcar_application/Models/productListModel.dart';

import '../Services/services.dart';

class ProductController extends GetxController{
  final api = API();
  final mapJson = ProductListModel().obs;
  ProductListModel get response => mapJson.value;

  callProduct(String productName)async{
    var response = await api.productList(productName);
    mapJson.value = ProductListModel.fromJson(response);
  }
}