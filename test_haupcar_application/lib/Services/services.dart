import 'package:dio/dio.dart';

class API {
  String baseUrl = "https://dummyjson.com/products/";
  final dio = Dio();

  categories()async{
    try {
      var response = await dio.get(baseUrl + 'categories');
      return response.data;
    } on DioException catch (e) {
      return e.message;
    }
  }

  productList(String productname)async{
    try {
      var response = await dio.get(baseUrl + 'category/' + productname);
      return response.data;
    } on DioException catch (e) {
      return e.message;
    }
  }
}