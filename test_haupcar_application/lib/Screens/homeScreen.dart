import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';
import 'package:test_haupcar_application/Controllers/categoriesController.dart';
import 'package:easy_localization/easy_localization.dart';
import '../Controllers/prdController.dart';
import '../listImage.dart';

class HomeScreen extends StatefulWidget {
  HomeScreen({super.key});

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  final categoriesCtrl = Get.put(CategoreiesController());
  final prdCtrl = Get.put(ProductController());

  
  @override
  void initState(){
    super.initState();
    categoriesCtrl.callCategories();
  }
  
  @override
  Widget build(BuildContext context) {
    return  Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: Colors.deepPurpleAccent,
        title: const Text('head',style: TextStyle(color: Colors.white,fontSize: 20),).tr(),
      ),
      body: body(),
      drawer: Drawer(
        child: Padding(
          padding: EdgeInsets.only(top: 30.0,left: 10.0),
          child: ListTile(
            title: const Text('changeLanguage').tr(),
            selected: true,
            onTap: (){
              setState(() {
                if(context.locale.languageCode == 'th'){
                  context.setLocale(const Locale('en', 'US'));
                  Get.updateLocale(Locale('en', 'US'));
                }else if(context.locale.languageCode == 'en'){
                  context.setLocale(const Locale('th', 'TH'));
                  Get.updateLocale(Locale('th', 'TH'));
                }
              });
              Get.back();
            },
          )
        ),
      ),
    );
  }

  Widget body(){
    return Obx(() =>GridView.builder(
      itemCount: categoriesCtrl.categoriesList.length,
      shrinkWrap: true,
      padding: EdgeInsets.only(top: 10,left: 10,right: 10,bottom: 10),
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        childAspectRatio:2.0,
        mainAxisSpacing: 5.0,
        crossAxisSpacing: 5.0,
        crossAxisCount: 2),
      itemBuilder: (context, index) {
        return categoryBox(index);
      },
    ));
  }

  Widget categoryBox(index){
    return GestureDetector(
      onTap: ()async{
        await prdCtrl.callProduct(categoriesCtrl.categoriesList[index]);
        Get.toNamed('/productList',arguments: categoriesCtrl.categoriesList[index]);
      },
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(8),
          color: Colors.grey.shade300
        ),
        padding: EdgeInsets.symmetric(horizontal: 7),
        child: Row(
          children: [
            Container(
              height: 50,
              width: 50,
              decoration: BoxDecoration(
                
              ),
              child: Image.asset(listPathIcon[index]),
            ),
            SizedBox(width: 10,),
            Expanded(child: Text(categoriesCtrl.categoriesList[index],overflow: TextOverflow.ellipsis,))
          ],
        ),
      ),
    );
  }
}