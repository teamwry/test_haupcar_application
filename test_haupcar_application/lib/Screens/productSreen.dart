import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';
import 'package:test_haupcar_application/Controllers/prdController.dart';

class ProductListScreen extends StatefulWidget {
   ProductListScreen({super.key});

  @override
  State<ProductListScreen> createState() => _ProductListScreenState();
}

class _ProductListScreenState extends State<ProductListScreen> {
  final prdCtrl = Get.put(ProductController());
  final nameItem = Get.arguments;
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: Colors.deepPurpleAccent,
        title: Text('$nameItem List',style: TextStyle(color: Colors.white,fontSize: 20),),
        leading: GestureDetector(
          onTap: (){
            Get.back();
          },
          child: Icon(Icons.arrow_back),
        ),
      ),
      body: body(),
    );
  }

  Widget body(){
    return ListView.builder(
      padding: EdgeInsets.only(left: 10,right: 10,bottom: 5,top: 5),
      shrinkWrap: true,
      itemCount: 5,
      itemBuilder: (context, index){
        return Obx(() =>Padding(
          padding: EdgeInsets.only(top: 5),
          child: Container(
            decoration: BoxDecoration(
              color: Colors.grey.shade300,
              borderRadius: BorderRadius.circular(8)
            ),
            padding: EdgeInsets.all(10),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  height: 100,
                  width: MediaQuery.sizeOf(context).width,
                  child: Image.network('${prdCtrl.mapJson.value.products![index].thumbnail}'),
                ),
                Row(
                  children: [
                    Text('id').tr(),
                    Expanded(child: Text(' : ${prdCtrl.mapJson.value.products![index].id}',style: TextStyle(overflow: TextOverflow.ellipsis),)),
                  ],
                ),
                Row(
                  children: [
                    Text('title').tr(),
                    Expanded(child: Text(' : ${prdCtrl.mapJson.value.products![index].title}',style: TextStyle(overflow: TextOverflow.ellipsis))),
                  ],
                ),
                Row(
                  children: [
                    Text('description').tr(),
                    Expanded(child: Text(' : ${prdCtrl.mapJson.value.products![index].description}',style: TextStyle(overflow: TextOverflow.ellipsis))),
                  ],
                ),
                Row(
                  children: [
                    Text('price').tr(),
                    Expanded(child: Text(' : ${prdCtrl.mapJson.value.products![index].price}',style: TextStyle(overflow: TextOverflow.ellipsis))),
                  ],
                ),
                Row(
                  children: [
                    Text('discountPercentage').tr(),
                    Expanded(child: Text(' : ${prdCtrl.mapJson.value.products![index].discountPercentage}',style: TextStyle(overflow: TextOverflow.ellipsis))),
                  ],
                ),
                Row(
                  children: [
                    Text('rating').tr(),
                    Expanded(child: Text(' : ${prdCtrl.mapJson.value.products![index].rating}',style: TextStyle(overflow: TextOverflow.ellipsis))),
                  ],
                ),
                Row(
                  children: [
                    Text('stock').tr(),
                    Expanded(child: Text(' : ${prdCtrl.mapJson.value.products![index].stock}',style: TextStyle(overflow: TextOverflow.ellipsis))),
                  ],
                ),
                Row(
                  children: [
                    Text('brand').tr(),
                    Expanded(child: Text(' : ${prdCtrl.mapJson.value.products![index].brand}',style: TextStyle(overflow: TextOverflow.ellipsis))),
                  ],
                ),
                Row(
                  children: [
                    Text('category').tr(),
                    Expanded(child: Text(' : ${prdCtrl.mapJson.value.products![index].category}',style: TextStyle(overflow: TextOverflow.ellipsis))),
                  ],
                ),
              ],
            ),
          ),
        ));
      });
  }
}